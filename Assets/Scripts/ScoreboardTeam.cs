﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreboardTeam : MonoBehaviour
{

    public Team team;
    public GameObject nameCard;
    public GameObject hammer;
    public List<GameObject> scoreCards;

    public int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        SetName();
        SetColor();
    }

    public void HasHammer(bool doesHaveHammer)
    {
        hammer.SetActive(doesHaveHammer);
    }

    public void SetScore()
    {
        score = 0;
        for (int i = 0; i < team.inningScores.Count; i++)
        {
            int inn = team.inningScores[i];
            score += inn;
            if (inn > 0)
            {
                GameObject scoreCard = scoreCards[score - 1];
                scoreCard.SetActive(true);
                scoreCard.GetComponentInChildren<TextMesh>().text = (i + 1).ToString();
            }
        }

    }

    private void SetName()
    {
        nameCard.GetComponent<TextMeshPro>().text = team.GetComponent<Team>().teamName;
        //todo resize text to fit long names on card
    }

    private void SetColor()
    {
        Color color = team.teamColor.GetColor("_Color");
        gameObject.GetComponent<MeshRenderer>().material.SetColor("_Color", color);
    }

}
