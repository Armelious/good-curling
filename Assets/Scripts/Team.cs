﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Team", menuName = "ScriptableObjects/Team")]

public class Team : MonoBehaviour
{

    public Game_Manager gameManager;

    public string teamName;
    public Material teamColor;

    public int teamScore = 0;
    public List<int> inningScores = new List<int>();

    public List<GameObject> stones;
    public List<GameObject> activeStones = new List<GameObject>();
    public GameObject currentStone = null;

    public int stonesThrown = 0;
    public bool allStonesThrown = false;
    public bool allStonesDone = false;

    public List<string> players;
    public string player;

    /// <summary>
    /// Activate the next stone
    /// </summary>
    public void TurnStart()
    {
        SetPlayer();

        currentStone = stones[stonesThrown];
        Stone_Manager stoneManager = currentStone.GetComponent<Stone_Manager>();
        stoneManager.enabled = true;
        stoneManager.SetStoneName(stonesThrown + 1);
        stoneManager.StartTurn();
    }

    /// <summary>
    /// Set the name of the currently players turn. Based on stone count.
    /// </summary>
    private void SetPlayer()
    {
        switch (stonesThrown + 1)
        {
            case 1:
            case 2:
                player = players[0];
                break;
            case 3:
            case 4:
                player = players[1];
                break;
            case 5:
            case 6:
                player = players[2];
                break;
            case 7:
            case 8:
                player = players[3];
                break;
            default:
                player = "Ninja";
                break;
        }
    }

    public string GetPlayer()
    {
        return player;
    }

    internal void AddActiveStone(GameObject stone)
    {
        activeStones.Add(stone);
    }

    internal void RemoveActiveStone(GameObject stone)
    {
        activeStones.Remove(stone);
    }

    /// <summary>
    /// The active Stone has finished moving. 
    /// If that were the 8th stone then all stones are done.
    /// Let the game manager know this teams turn is done.
    /// </summary>
    public void TurnEnd()
    {
        stonesThrown++;
        if (stonesThrown == 8)
        {
            allStonesThrown = true;
        }
        currentStone = null;
        StartCoroutine(gameManager.TeamTurnEnd());
        
    }

    /*
    /// <summary>
    /// Check each stone if the Stone_Manager is enabled
    /// </summary>
    /// <returns>bool if any stone's manageger is currently running</returns>
    public void AllStonesDone()
    {
        foreach (GameObject stone in stones)
        {
            Stone_Manager stoneManager = stone.GetComponent<Stone_Manager>();
            StartCoroutine(WaitForStoneToFinish(stone));

        }

        
        Debug.Log(teamName + ": All stones are done");
    }


    IEnumerator WaitForStoneToFinish(GameObject stone)
    {
        yield return new WaitUntil(() => stone.GetComponent<Stone_Manager>().isActiveAndEnabled == false);
    }
    */

    public int GetStoneCount()
    {
        return stones.Count;
    }

    /// <summary>
    /// Return a list of stones that are touching the ring
    /// </summary>
    /// <returns>List<GameObject> of the stones that ar touching</GameObject></returns>
    public List<GameObject> GetCountingStones()
    {
        List<GameObject> countingStones = new List<GameObject>();
        foreach (GameObject stone in stones)
        {
            if (stone.GetComponent<Stone_Manager>().GetDistanceToRing() < 1.975f)
            {
                countingStones.Add(stone);
            }
        }
        /*order list by distance to button

        countingStones.Sort(delegate(GameObject s1, GameObject s2) 
        {
            return s1.GetComponent<Stone_Manager>().GetDistanceToRing().CompareTo(s2.GetComponent<Stone_Manager>().GetDistanceToRing());
        });
        */
        Debug.Log(gameObject.name + " counting stones: " + countingStones);
        return countingStones;
    }

    //todo move this to the score manager
    public void AddUpScore()
    {
        teamScore = 0;
        foreach (int inning in inningScores)
        {
            teamScore += inning;
        }
    }

    /// <summary>
    /// A new inning is starting, make sure all the stones are reset.
    /// Move stones to Start position and booleans to false.
    /// </summary>
    public void ResetStones()
    {
        foreach (GameObject stone in stones)
        {
            stone.GetComponent<Stone_Manager>().Reset();
        }
        stonesThrown = 0;
        allStonesThrown = false;
        allStonesDone = false;
    }

}
