﻿using UnityEngine;
using System;

public class StoneThrower : MonoBehaviour
{

    public Rigidbody stone;
    public DragObject controller;
    public GameObject target;

    public State activeState;

    public float chargePower;
    public float power;

    public Vector3 startPosition = Vector3.zero;
    public float zDistance;
    
    void OnEnable()
    {
        Reset();
    }

    public void Reset()
    {
        Debug.Log("StoneThrower RESET");
        activeState = State.IDLE;
        if (stone)
        {
            gameObject.transform.position = stone.transform.position;
            startPosition = transform.position;
        }
        power = 0;
        chargePower = 0;
    }

    public void SetStone(GameObject stone)
    {
        this.stone = stone.GetComponent<Rigidbody>();
        gameObject.transform.position = stone.transform.position;
        startPosition = transform.position;
    }

    public enum State
    {
        IDLE, CHARGING, THROWING
    }

    // Update is called once per frame
    void Update()
    {

        if (activeState == State.IDLE)
        {
            if (controller.dragging)
            {
                activeState = State.CHARGING;
            }
        }
        else
        {
            if (!controller.dragging) //released
            {
                Reset();
            }
            else if (controller.direction.z > 0.01) //throwing
            {
                activeState = State.THROWING;
            }
            else if (controller.direction.z < -0.01) //charging
            {
                activeState = State.CHARGING;
            }

            //spin
            Vector3 spin = new Vector3(0, controller.direction.x, 0);
            stone.AddTorque(spin * 5, ForceMode.Force);
        }

        if (activeState == State.CHARGING)
        {
            chargePower += Math.Abs(controller.direction.z) * 10;
        }
        else if (activeState == State.THROWING)
        {
            Vector3 targetVector = (target.transform.position - stone.transform.position).normalized;

            //release charge
            if (stone.velocity.magnitude < chargePower)
            {
                stone.AddForce(targetVector * chargePower, ForceMode.Acceleration);
            }

            //forward push
            power += controller.direction.z;
            stone.AddForce(targetVector * power, ForceMode.Acceleration);
        }

    }

    /// <summary>
    /// The distance along the z axis of the stone controller and the stone start position
    /// </summary>
    /// <returns></returns>
    public float GetZDistance()
    {
        zDistance = gameObject.transform.position.z - startPosition.z;
        return zDistance;
    }

}
