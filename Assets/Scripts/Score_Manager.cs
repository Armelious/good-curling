﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score_Manager : MonoBehaviour
{

    public int inning = 1;

    public GameObject inningCard;
    public GameObject blankCard;

    public ScoreboardTeam team1;
    public ScoreboardTeam team2;

    public void Start()
    {
        DisplayInning();
    }

    public void NextInning()
    {
        inning++;
        DisplayInning();
    }

    public void DisplayInning()
    {
        inningCard.GetComponentInChildren<TextMesh>().text = inning.ToString();
    }

    public void InningBlanked()
    {
        blankCard.SetActive(true);
        blankCard.GetComponentInChildren<TextMesh>().text = inning.ToString();
    }

    public void SetTeamScores()
    {
        team1.SetScore();
        team2.SetScore();
    }

    public void SetHammer(Team team)
    {
        if(team.Equals(team1.team))
        {
            team1.HasHammer(true);
            team2.HasHammer(false);
        }
        else
        {
            team1.HasHammer(false);
            team2.HasHammer(true);
        }
    }

}
