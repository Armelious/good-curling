﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Game_Manager : MonoBehaviour {

    public Score_Manager scoreManager;
    public Camera_Manager cameraManager;

    public PhysicMaterial iceMaterial;
    public Transform iceTiles;

    public List<Team> teams;
    private Team currentTeam;

    void Start () {
        SetFirstTeamOnCoinFlip();
        SetPhysicsMaterial(0.1f, 0.12f);
        TeamStartTurn();
        cameraManager.SwitchPosition(Camera_Manager.CameraPositions.SCOREBOARD);
    }

    /// <summary>
    /// Flip a coin to determine who goes first
    /// </summary>
    private void SetFirstTeamOnCoinFlip()
    {
        int flip = Random.Range(0, 2);
        SetCurrentTeam(teams[flip]);
        //Set who has hammer on scoreboard
        if (flip.Equals(0))
        {
            Debug.Log(teams[1].teamName + " won the hammer");
            scoreManager.SetHammer(teams[1]);
        }
        else
        {
            Debug.Log(teams[0].teamName + " won the hammer");
            scoreManager.SetHammer(teams[0]);
        }
    }

    public void TeamStartTurn()
    {
        currentTeam.TurnStart();
        cameraManager.stoneThrower.SetStone(currentTeam.currentStone);
    }

    /// <summary>
    /// The current teams active stone has finished.
    /// Make sure all stones have finished moving.
    /// </summary>
    internal IEnumerator TeamTurnEnd()
    {
        while (teams[0].activeStones.Count > 0 || teams[1].activeStones.Count > 0)
        {
            yield return new WaitForSeconds(.2f);
        }
        TeamTurnDone();
    }

    /// <summary>
    /// If all stones have been thrown then end the inning.
    /// Otherwise switch to the other team.
    /// </summary>
    public void TeamTurnDone()
    {
        //EnableCamera(houseCamera);
        cameraManager.SwitchPosition(Camera_Manager.CameraPositions.SKIP);
        
        //all stones are thrown
        if (teams[0].allStonesThrown && teams[1].allStonesThrown)
        {
            ScoreInning();

            //game is over
            if (scoreManager.inning == 8)
            {
                Team winningTeam = GetWinningTeam();
                print(winningTeam.teamName + " is the winning team with " + winningTeam.teamScore + " points!");
                //todo end the game
            }
        }
        else
        {
            SwitchTeam();
            TeamStartTurn();
        }
    }

    private void SetCurrentTeam(Team team)
    {
        currentTeam = team;
    }

    private void SwitchTeam()
    {
        currentTeam = currentTeam.Equals(teams[0]) ? teams[1] : teams[0];
    }
    
    /// <summary>
    /// Get a list of stones that are touching the ring
    /// Order those by closest to center
    /// </summary>
    private void ScoreInning()
    {
        cameraManager.SwitchPosition(Camera_Manager.CameraPositions.SCOREBOARD);

        //todo try to move some of this to the Score_Manager

        //sort the list by closest to the ring
        List<GameObject> aStones = teams[0].GetCountingStones();
        List<GameObject> bStones = teams[1].GetCountingStones();

        List<GameObject> stones = aStones.Concat(bStones).ToList();

        if (stones.Count > 0)
        {
            Debug.Log("Counting Stones");
            stones.Sort((s1, s2) => s1.GetComponent<Stone_Manager>().GetDistanceToRing()
                .CompareTo(s2.GetComponent<Stone_Manager>().GetDistanceToRing()));

            Team scoringTeam = stones[0].GetComponent<Stone_Manager>().myTeam;
            Debug.Log(scoringTeam.name + " has scored");
            int countingStones = 0;
            foreach (GameObject stone in stones)
            {
                if (stone.GetComponent<Stone_Manager>().myTeam.Equals(scoringTeam))
                {
                    Debug.Log("Scored with stone " + stone.name + " with distance " + stone.GetComponent<Stone_Manager>().GetDistanceToRing());
                    countingStones += 1;
                }
                else
                {
                    break;
                }
            }

            //Add the scores for the inning to the teams.
            foreach (Team t in teams)
            {
                if (t.Equals(scoringTeam))
                {
                    t.inningScores.Add(countingStones);
                    Debug.Log(t.teamName + " team scored: " + countingStones);
                }
                else
                {
                    t.inningScores.Add(0);
                    Debug.Log(t.teamName + " team scored: 0");
                    scoreManager.SetHammer(t);
                }
                t.AddUpScore();
                t.ResetStones();
            }
            //start next inning with the winning team going first;
            SetCurrentTeam(scoringTeam);
        }
        else
        {
            Debug.Log("Blanked the end");
            foreach (Team t in teams)
            {
                t.inningScores.Add(0);
                t.ResetStones();
            }
            scoreManager.InningBlanked();
            SwitchTeam();
        }
        scoreManager.SetTeamScores();
        scoreManager.NextInning();
        RotateSides();
        TeamStartTurn();
    }

    /// <summary>
    /// Rotate the ice to similate coming back the other way after each end
    /// </summary>
    private void RotateSides()
    {
        float y = iceTiles.eulerAngles.y;
        if ( y == 0)
        {
            iceTiles.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            iceTiles.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    //todo use the score manager for this
    private Team GetWinningTeam()
    {
        if (teams[0].teamScore > teams[1].teamScore)
        {
            return teams[0];
        } else
        {
            return teams[1];
        }
        //ToDo determine if there was a tie
    }

    private void SetPhysicsMaterial(float dFriction, float sFriction)
    {
        iceMaterial.dynamicFriction = dFriction;
        iceMaterial.staticFriction = sFriction;
    }

}
