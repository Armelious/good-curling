﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceTile : MonoBehaviour {

    public float myDynamicFriction;
    public float myStaticFriction;
    public float increment;
    public PhysicMaterial myPhyMaterial;

	
    //warm up the ice
    public void WarmUpIce()
    {
        if (myDynamicFriction > increment)
        {
            myDynamicFriction = myDynamicFriction - increment;
            myStaticFriction = myStaticFriction - increment;
        }
    }

    public void UpdatePhysicsMaterial()
    {
        myPhyMaterial.dynamicFriction = myDynamicFriction;
        myPhyMaterial.staticFriction = myStaticFriction;
    }
}
