﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camera_Manager : MonoBehaviour {

    public Transform houseCamera;
    public Transform scoreboardCamera;
    public Transform skipCamera;
    public Transform stoneCamera;

    public CameraPositions cameraPosition;

    public Button upButton;
    public Button downButton;
    public StoneThrower stoneThrower;
    public GameObject speedSign;

    public Vector3 stoneOffset;

    public List<Team> teams;
    public List<GameObject> activeStones;

    private Vector3 target;
    private float speed;

    private void Start()
    {
        upButton.onClick.AddListener(UpButtonClicked);
        downButton.onClick.AddListener(DownButtonClicked);
    }

    void Update()
    {
        if (cameraPosition == CameraPositions.STONE)
        {
            SetActiveStones();
            if (activeStones.Count > 0)
            {
                //When stone thrower is pulled back lift up camera and push towards stone while throwing.
                Vector3 chargeOffset = Vector3.zero;
                float zDistance = stoneThrower.GetZDistance();
                if (stoneThrower.chargePower > 0 && stoneThrower.activeState != StoneThrower.State.IDLE)
                {
                    //prevent the zdistance from being greater than 1
                    if (zDistance > 1)
                    {
                        zDistance /= zDistance;
                    }
                    chargeOffset = ((GetActiveStonesAverage() - transform.position).normalized) * zDistance;
                }

                target = GetActiveStonesAverage() + stoneOffset + chargeOffset;
                float dist = Vector3.Distance(transform.position, target);
                speed = dist * 5f;

                //speed of active stone
                if (activeStones[0].GetComponent<Rigidbody>().velocity.magnitude > 0.01f)
                {
                    speedSign.GetComponentInChildren<Text>().text =
                        System.Math.Round(activeStones[0].GetComponent<Rigidbody>().velocity.magnitude, 2).ToString();
                }
                
            }
        }
    }

    /// <summary>
    /// Ensure the step is consistent reguardless of framerates
    /// </summary>
    void FixedUpdate()
    {
        // Move our position a step closer to the target.
        float step = speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, target, step);
    }

    private void SetActiveStones()
    {
        List<GameObject> tempList = new List<GameObject>();
        foreach (Team team in teams)
        {
            tempList.AddRange(team.activeStones);
        }

        if (!activeStones.Equals(tempList))
        {
            activeStones = tempList;
        }
    }

    public enum CameraPositions
    {
        HOUSE, SCOREBOARD, SKIP, STONE
    }

    /// <summary>
    /// Move the camera to a position with rotation and field of view.
    /// Update the GUI up & down button text for the next position.
    /// </summary>
    /// <param name="cameraPosition">House, Scoreboard, Skip, or Stone</param>
    public void SwitchPosition(CameraPositions cameraPosition)
    {
        Debug.Log("Moving camera to: " + cameraPosition);
        this.cameraPosition = cameraPosition;
        if (cameraPosition == CameraPositions.HOUSE)
        {
            target = houseCamera.transform.position;
            StartCoroutine(RotateToView(houseCamera.transform.rotation));
            speed = 20f;
            gameObject.GetComponent<Camera>().fieldOfView = houseCamera.GetComponent<Camera>().fieldOfView;
            upButton.GetComponentInChildren<Text>().text = "Score";
            downButton.GetComponentInChildren<Text>().text = "Stone";
            stoneThrower.enabled = false;
        }
        else if (cameraPosition == CameraPositions.SCOREBOARD)
        {
            target = scoreboardCamera.transform.position;
            StartCoroutine(RotateToView(scoreboardCamera.transform.rotation));
            speed = 20f;
            gameObject.GetComponent<Camera>().fieldOfView = scoreboardCamera.GetComponent<Camera>().fieldOfView;
            upButton.GetComponentInChildren<Text>().text = "Skip";
            downButton.GetComponentInChildren<Text>().text = "House";
            stoneThrower.enabled = false;
        }
        else if (cameraPosition == CameraPositions.SKIP)
        {
            target = skipCamera.transform.position;
            StartCoroutine(RotateToView(skipCamera.transform.rotation));
            speed = 20f;
            gameObject.GetComponent<Camera>().fieldOfView = skipCamera.GetComponent<Camera>().fieldOfView;
            upButton.GetComponentInChildren<Text>().text = "Stone";
            downButton.GetComponentInChildren<Text>().text = "Score";
            stoneThrower.enabled = false;
        }
        else if (cameraPosition == CameraPositions.STONE)
        {
            StartCoroutine(RotateToView(stoneCamera.transform.rotation));
            gameObject.GetComponent<Camera>().fieldOfView = stoneCamera.GetComponent<Camera>().fieldOfView;
            upButton.GetComponentInChildren<Text>().text = "House";
            downButton.GetComponentInChildren<Text>().text = "Skip";
            stoneThrower.enabled = true;
        }
    }

    private void UpButtonClicked()
    {
        if (cameraPosition == CameraPositions.HOUSE)
        {
            SwitchPosition(CameraPositions.SCOREBOARD);
        }
        else if (cameraPosition == CameraPositions.SCOREBOARD)
        {
            SwitchPosition(CameraPositions.SKIP);
        }
        else if (cameraPosition == CameraPositions.SKIP)
        {
            SwitchPosition(CameraPositions.STONE);
        }
        else if (cameraPosition == CameraPositions.STONE)
        {
            SwitchPosition(CameraPositions.HOUSE);
        }
    }

    private void DownButtonClicked()
    {
        if (cameraPosition == CameraPositions.HOUSE)
        {
            SwitchPosition(CameraPositions.STONE);
        }
        else if (cameraPosition == CameraPositions.STONE)
        {
            SwitchPosition(CameraPositions.SKIP);
        }
        else if (cameraPosition == CameraPositions.SKIP)
        {
            SwitchPosition(CameraPositions.SCOREBOARD);
        }
        else if (cameraPosition == CameraPositions.SCOREBOARD)
        {
            SwitchPosition(CameraPositions.HOUSE);
        }
    }

    private IEnumerator RotateToView(Quaternion targetRotation)
    {
        for (float t = 0.0f; t < 1.0; t += Time.deltaTime * 10f)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, t);
            yield return null;
        }
        transform.rotation = targetRotation;
    }

    /// <summary>
    /// Get the average point between all of the active stones.
    /// </summary>
    /// <returns>Vector3 the middle point bewtween all active stones</returns>
    private Vector3 GetActiveStonesAverage()
    {
        Vector3 middle = Vector3.zero;
        foreach (GameObject stone in activeStones)
        {
            middle += stone.transform.position;
        }
        //take average:
        middle /= activeStones.Count;
        return middle;
    }

}
