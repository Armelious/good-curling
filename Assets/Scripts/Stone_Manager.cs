﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stone_Manager : MonoBehaviour {

    public Team myTeam;
    public Transform ring;

    public Vector3 startPosition;
    public Vector3 throwPosition;
    public Vector3 outPosition;

    public bool isThrown = false;
    public bool isOut = false;
    public bool myTurn = false;

    private Rigidbody m_Rigidbody;
    public float distanceToRing;
   
    // Use this for initialization
    void Start () {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    public void StartTurn()
    {
        myTurn = true;
        MoveToPosition(Positions.THROW);
    }

    public void Reset()
    {
        MoveToPosition(Positions.START);
        isThrown = false;
        isOut = false;
        myTurn = false;
        enabled = false;
    }

    public void SetTeam(Team team)
    {
        myTeam = team;
        SetTeamColor();
    }

    //set the color of the first material.
    public void SetTeamColor()
    {
        GetComponent<Renderer>().material = myTeam.teamColor;
    }

    public void SetStoneName(int count)
    {
        name = "Stone_" + myTeam.teamName + "_" + myTeam.GetPlayer() + "_" + count;
    }

    void OnEnable()
    {
        myTeam.AddActiveStone(gameObject);
    }

    void OnDisable()
    {
        myTeam.RemoveActiveStone(gameObject);
    }

    // Update is called once per frame
    void Update () {

        SetDistanceToRing();

        if (isThrown)
        {
            if (m_Rigidbody.IsSleeping())
            {
                CheckIfStoneIsOut();
                EndTurn();
            }
            else //not a sleep
            {
                //Determine if the stone has stopped moving.
                if (m_Rigidbody.velocity.magnitude < 0.01f) //reduce this?
                {
                    m_Rigidbody.Sleep();
                }
                WarmIce();
                CurlStone();
            }
        }
        else //Throw the stone
        {
            if (transform.position.z > 6.25f)
            {
                isThrown = true;
            }

            /*
            //push the stone forward
            if (Input.GetKey(KeyCode.UpArrow) && broom)
            {
                float appliedForce = (power + 0.2f) * 10;
                m_Rigidbody.AddForce((broom.transform.position - transform.position).normalized * appliedForce, ForceMode.Acceleration);
            }

            //add rotation
            if (Input.GetKey(KeyCode.RightArrow))
            {
                m_Rigidbody.AddTorque(transform.up * 0.1f, ForceMode.Force);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                m_Rigidbody.AddTorque(-transform.up * 0.1f, ForceMode.Force);
            }

            */
        }
    }

    /// <summary>
    /// When the stone is done then check to see if it passed the hog and did not pass the back line.
    /// If it did then move it out.
    /// </summary>
    /// <returns>bool if the stone is out</returns>
    private bool CheckIfStoneIsOut()
    {
        //Stone didn't make it past the hog line or past the back line
        if (transform.position.z < 28.45f || transform.position.z > 36.73)
        {
            StoneIsOut();
            return true;
        }
        return false;
    }

    private void EndTurn()
    {
        Debug.Log(gameObject.name + " is Done");
        if (myTurn)
        {
            myTurn = false;
            myTeam.TurnEnd();
        }
        enabled = false;
    }

    private void CurlStone()
    {
        float scaleAmount;
        if (m_Rigidbody.velocity.magnitude >= 8f)
        {
            scaleAmount = 0.01f;
        }
        else if (m_Rigidbody.velocity.magnitude >= 4f)
        {
            scaleAmount = 0.04f;
        }
        else if (m_Rigidbody.velocity.magnitude >= 2f)
        {
            scaleAmount = 0.08f;
        }
        else
        {
            scaleAmount = 0.1f;
        }
        //add velocity in the turn direction. which seems to slow downt he rock, add a touch of forward motion too.
        float sp = m_Rigidbody.angularVelocity.y * scaleAmount;
        float fwd = sp * 0.1f;
        m_Rigidbody.AddForce(sp, 0f, fwd, ForceMode.Acceleration);
    }

    private void WarmIce()
    {
        //int layerMask = LayerMask()
        RaycastHit hit;
        Debug.DrawRay(transform.position, Vector3.down * 1f);
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1f, 1 << 9)) //9 is the layer: Ice_Tile
        {
            GameObject iceTile = hit.transform.gameObject;
            iceTile.GetComponent<IceTile>().UpdatePhysicsMaterial();
            iceTile.GetComponent<IceTile>().WarmUpIce();
        }
    }

    //returns -1 when to the left, 1 to the right, and 0 for forward/backward
    public float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return 1f;
        }
        else if (dir < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }

    //if the stone hits the boundries then mark it as out so it can be destroyed
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Boundries")
        {
            StoneIsOut();
        }

        if (collision.gameObject.tag == "Stones" && isThrown)
        {
            Debug.Log(collision.gameObject.name + " hit");
            collision.gameObject.GetComponent<Stone_Manager>().enabled = true;
        }
    }

    //return the distance to the target ring
    public void SetDistanceToRing()
    {
        distanceToRing = Vector3.Distance(transform.position, ring.transform.position);
    }

    public float GetDistanceToRing()
    {
        return distanceToRing;
    }

    /// <summary>
    /// When a stone is out then Move it to the Out position
    /// </summary>
    private void StoneIsOut()
    {
        isOut = true;
        m_Rigidbody.velocity = Vector3.zero;
        MoveToPosition(Positions.OUT);
        EndTurn();
    }

    public enum Positions {
        START,
        THROW,
        OUT
    }
    public void MoveToPosition(Positions position)
    {
        Debug.Log("Moving " + gameObject.name + " to " + position + " position");
        switch (position)
        {
            case Positions.START:
                {
                    gameObject.transform.position = startPosition;
                    break;
                }
            case Positions.THROW:
                {
                    gameObject.transform.position = throwPosition;
                    break;
                }
            case Positions.OUT:
                {
                    gameObject.transform.position = outPosition;
                    //Debug.Log(gameObject.name + "Moved to Out Position");
                    break;
                }
            default:
                Debug.LogError("Need to select one of Postions emum:");
                break;
        }
    }

}
