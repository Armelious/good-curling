﻿using UnityEngine;

public class DragObject : MonoBehaviour
{
    public bool dragging = false;
    public Vector3 direction = Vector3.zero;

    private Plane plane;
    private Vector3 dragStart = Vector3.zero;
    private Vector3 offset = Vector3.zero;

    private void OnMouseDown()
    {
        plane = new Plane(Vector3.up, Vector3.up * transform.position.y); // ground plane
        //get offset of mouse postion to the center of object.
        offset = transform.position - GetMousePosition();
        dragStart = transform.position;
        dragging = true;
    }

    private void OnMouseDrag()
    {
        transform.position = GetMousePosition() + offset;
        direction = transform.position - dragStart;
        dragStart = transform.position;
    }

    private void OnMouseUp()
    {
        dragging = false;
    }

    /// <summary>
    /// Fire a Ray from the camera towards the Mouse.
    /// Get the point that the Ray intersects with the Plane.
    /// </summary>
    /// <returns>Vector3 Mouse Point</returns>
    private Vector3 GetMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // the distance from the ray origin to the ray intersection of the plane
        float distance;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
